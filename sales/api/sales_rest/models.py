from django.db import models
from django.urls import reverse

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.CharField(max_length=12) #Needs xxx-xxx-xxxx format, Extension (TRY using PhoneNumberField)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin}"

    # def get_api_url(self):
    #     return reverse("api_automobile", kwargs={"vin": self.vin})

class Sale(models.Model):
    price = models.CharField(max_length=14)   #$10,000,000.00 ($10mil is 14 char)
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    salesperson= models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer=models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
