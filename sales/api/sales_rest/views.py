from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# Create your views here.
from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder


import requests
import json

class SalespeopleEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "address",
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SalesEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "salesperson",
        "customer",
        "id",
    ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespeopleEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()

        return JsonResponse(
           {"salespeople":salespeople},
            SalespeopleEncoder,
            False,
        )
    else: #request.method== "POST"
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)

            return JsonResponse(
                salespeople,
                SalespeopleEncoder,
                False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the Salesperson"},
                status=400
            )
            return response

@require_http_methods(["DELETE"])
def delete_salespeople(request, id):
    if request.method == "DELETE":

        try:
            count, _ = Salesperson.objects.get(id=id).delete()

        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message: ": "Salesperson does not exist"},
                status=404,
            )

        return JsonResponse(
            {"delete": count>0}
        )

@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()

        return JsonResponse(
            {"customers": customers},
            CustomerEncoder,
            False,
        )
    else: #request.method == "POST"
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)

        return JsonResponse(
            customer,
            CustomerEncoder,
            False,
        )

@require_http_methods(["DELETE"])
def delete_customer(request, id):
    if request.method == "DELETE":

        try:
            count, _ = Customer.objects.get(id=id).delete()

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message: ": "Customer does not exist"},
                status=404,
            )

        return JsonResponse(
            {"delete": count>0}
        )

@require_http_methods(["GET","POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()

        return JsonResponse(
            {"sales": sales},
            SalesEncoder,
            False
        )
    else: #request.method == "POST"

        content = json.loads(request.body)

        try:
            auto_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=auto_vin)

            if automobile.sold is False:
                automobile.sold = True
                automobile.save()

            content["automobile"] = automobile

            inventory_url = f"http://project-beta-inventory-api-1:8000/api/automobiles/{auto_vin}/"
            info = '{"sold": "True"}'
            requests.put(inventory_url, info)

            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message: ": "Invalid VIN"},
                status=400
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message: ": "Invalid Salesperson ID"},
                status=400
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message: ": "Invalid Customer ID"},
                status=400,
            )

        sale = Sale.objects.create(**content)

        return JsonResponse(
            sale,
            SalesEncoder,
            False
        )

@require_http_methods(["DELETE"])
def delete_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)

            if sale.automobile.sold is True:
                sale.automobile.sold = False
                sale.save()

            auto_vin = sale.automobile.vin

            inventory_url = f"http://project-beta-inventory-api-1:8000/api/automobiles/{auto_vin}/"
            info = '{"sold": "False"}'
            requests.put(inventory_url, info)

            count, _ = Sale.objects.get(id=id).delete()

        except Sale.DoesNotExist:
            return JsonResponse(
                {"message: ": "Sale does not exist"},
                status=404,
            )

        return JsonResponse(
            {"delete: ": count>0}
        )
