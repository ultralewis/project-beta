import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function SalesForm ({getAutomobiles, getSales, salespeople, customers}) {

    const navigate = useNavigate();

    const [autos, setUnsoldAutos] = useState([]);

    const getUnsoldAutos = async () => {
        const unsoldAutosUrl = "http://localhost:8100/api/automobiles/create/";
        const response = await fetch(unsoldAutosUrl);

        if (response.ok) {
        const { autos } = await response.json();
        setUnsoldAutos(autos);
        };
    };

    const [automobile, setAutomobile] = useState("");
    const [salesperson, setSalesperson] = useState("");
    const [customer, setCustomer] = useState("");
    const [price, setPrice] = useState("");

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    };

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data={};

        data.price = price;
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;

        const salesUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(data),
            header: {
                "Content-Type":"application/json",
            },
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log("newSale", newSale);

            setAutomobile("");
            setSalesperson("");
            setCustomer("");
            setPrice("");

            getSales();
            getAutomobiles();
            navigate("/sales");
        }
    }

    useEffect(() => {
        getUnsoldAutos();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="record-sale-form">
              <div className="mb-3">
                <label htmlFor="vin" className="form-label">Automobile VIN</label>
                <select value={automobile} onChange={handleAutomobileChange} required id="vin" name="vin" className="form-select">
                  <option value="">Choose an automobile VIN...</option>
                  {autos.map(auto => {
                    return (
                        <option key= {auto.vin} value= {auto.vin}>
                            {auto.vin}
                        </option>
                    );
                  })};
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="salesperson" className="form-label">Salesperson</label>
                <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                  <option value="">Choose a salesperson...</option>
                  {salespeople.map(salesperson => {
                    return (
                        <option key= {salesperson.id} value= {salesperson.id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    );
                  })};
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="customer" className="form-label">Customer</label>
                <select value={customer} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {customers.map(customer => {
                    return (
                        <option key= {customer.id} value= {customer.id}>
                            {customer.first_name} {customer.last_name}
                        </option>
                    );
                  })};
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="Price">Price</label>
                <input value={price} onChange={handlePriceChange} placeholder="Format: $xx,xxx.xx" required type="text" name="price" id="rice" className="form-control"/>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
};

export default SalesForm;
