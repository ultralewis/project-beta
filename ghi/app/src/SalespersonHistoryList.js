import React from "react";
import { useState } from "react";

function SalespersonHistoryList({salespeople, sales}) {

  const [salesperson, setSalesperson] = useState("");

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const salesHistory = sales.filter((sale) => sale.salesperson.id === Number(salesperson));

  return (
    <div>
      <h1>Salesperson History</h1>
      <div className="mb-3">
        <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
          <option value="">Choose a Salesperson</option>
          {salespeople.map((salesperson) => {
            return (
              <option key={salesperson.id} value={salesperson.id}>
                {salesperson.first_name} {salesperson.last_name}
              </option>
            );
          })};
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN </th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
            {salesHistory.map((sale) => {
              return (
                <tr key={sale.id}>
                  <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                  <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.price}</td>
                </tr>
              )
            })}
        </tbody>
      </table>
    </div>
  );
};

export default SalespersonHistoryList;
