import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const ModelForm = ({manufacturers}) => {

    const navigate = useNavigate();

      const[data, setData] =useState({
        name:"",
        picture_url: "",
        manufacturer_id: "",
      });

      const handleChangeName = (event) => {
        const type = event.target.type;
        const name = event.target.name;
        const value = event.target.value
        setData(curData => ({
            ...curData,
            [name] : value
        }));
      }

       const postData = async (e)=> {
        e.preventDefault();

        const url = 'http://localhost:8100/api/models/'

        const options = {
          method: 'post',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify(data)
        };

        const response = await fetch(url, options);

        if (response.ok) {
          navigate(`/models`);
      }else{
        console.log("Error posting")
      }
      }

  return (
    <div className="container mt-5">
    <h1 className="text-center mb-4">Add A Model</h1>
      <form onSubmit={postData} className="">
    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="text"  name="name" className="form-control" id="modelname" aria-describedby="shoeModel" placeholder=" Model name" />
    </div>

    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="url"  name="picture_url" className="form-control" id="modelPicture" aria-describedby="modelpicture" placeholder="picture url" />
    </div>

    <div className="dropdown mb-2 ">
    <label className="m-2"> </label>
    <select id="a" className="container" onChange={handleChangeName} name="manufacturer_id">
      <option value="" hidden>Manufacturer</option>
    {manufacturers.map(manufacturer=> {return(
       <option value={manufacturer.id} key={manufacturer.id}>{manufacturer.name}</option>
    )})}
      </select>
     </div>
    <button  type="submit" className="btn mt-2 btn-primary ">Submit</button>
  </form>

  </div>

  )
  }

  export default ModelForm
