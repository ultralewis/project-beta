import React from "react";
import ModelList from "./DisplayCarModels";
import { useEffect, useState} from "react";

const AllModelss = () => {
    const [models, setModels] = useState([])
    const modData =async () => {
        const url = 'http://localhost:8100/api/models/'
        const getData = await fetch(url);
        const data = await getData.json();
        setModels(data.models)
    }
    useEffect(()=>{
        modData()
    },[])
    return(
        <div className="container">
            <div className="row">
            {/* <div className="col"> */}

        <ModelList models={models} refresh={modData}/>
        {/* </div> */}
        </div>
        </div>
    )
}


export default AllModelss
