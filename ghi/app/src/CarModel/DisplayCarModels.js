import React from "react";
import { useState, useEffect } from "react";
import allModelss from "./FetchCarModels";

const ModelList = ({models}) => {

    return(

        models.map((model, id) => {
            return(
                <div key={id} className= "m-3 text-center card" style={{width: 250}}>
                <img className="card-img-top" src={model.picture_url} alt="Card image cap" />
                <div className="card-body">
                  <h5 className="card-title">{model.name}</h5>
                  <h6 className="inset card-subtitle m-2 card-text text-muted" >{model.manufacturer.name} </h6>
                </div>
              </div>
            )
        })

    )

}

export default ModelList
