import React from "react";
import { useEffect, useState} from "react";

export const GetTechs = () => {
    const [techs, settechs] = useState([])

    const techData =async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const getData = await fetch(url);
        const data = await getData.json();

        settechs(data.techs)
    }
    useEffect(()=>{
        techData()
    },[])

    return(
        techs
    )
}
