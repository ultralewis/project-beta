
import { GetTechs } from "./FetchTechs"
export const TechListView = () => {
    const techs = GetTechs()

    const headers=[
        'Item #',
        'First Name',
        'Last Name',
        'Employee ID'
    ]

    return (
      <div>
        <h1>Technicians</h1>
        <table className="table">
          <thead>
            <tr>
              {headers.map((header, index) => {
                return (
                  <th key={index} scope="col">
                    {header}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {techs.map((tech, index) => {
              return (
                <tr key={tech.id}>
                  <th scope="row">{index + 1}</th>
                  <td>{tech.first_name}</td>
                  <td>{tech.last_name}</td>
                  <td>{tech.employee_id}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
}
