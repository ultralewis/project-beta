
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export const CreateTech = ()=> {
  const navigate = useNavigate();

    const[data, setData] =useState({
      first_name:"",
      last_name: "",
      employee_id: "",
    });

    const handleChangeName = (event) => {
      const type = event.target.type;
      const name = event.target.name;
      const value = event.target.value
      setData(curData => ({
          ...curData,
          [name] : value
      }));
    }

    const postData = async (e)=> {
      e.preventDefault();

      const url = `http://localhost:8080/api/technicians/`

      const options = {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(data)
      };

      const response = await fetch(url, options);

      if (response.ok) {
        navigate("/techs");

    }else{
      console.log("Error posting")
    }
  }
  return (
    <div className="container mt-5">
    <h1 className="text-center mb-4">Add a Technician </h1>
      <form onSubmit={postData} className="">
    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="text"  name="first_name" className="form-control" id="firstname" aria-describedby="lastname" placeholder="First Name" />
    </div>

    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="text"  name="last_name" className="form-control" id="lastname" aria-describedby="lastname" placeholder="Last Name" />
    </div>

    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="number"  name="employee_id" className="form-control" id="employee-id" aria-describedby="employeeid" placeholder="Employee ID..." />
    </div>
    <button  type="submit" className="btn mt-2 btn-primary ">Create</button>
    </form>
    </div>

  )
  }
