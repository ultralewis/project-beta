import { useNavigate } from "react-router-dom"
import { useEffect } from "react"

function ServiceListView ({autos, getServices, appointments}) {

    const headers=[
        'Item #',
        'Vin',
        'Is VIP?',
        'Customer',
        'Date',
        'Time',
        'Technician',
        'Reason',
        'Finish',
        'Cancel',
    ]

    const submittedAppointments = appointments.filter((appointment) => appointment.status.status === "Waiting")

    const navigate = useNavigate()

    const x = []
    autos.map(a => x.push(a.vin))

    const setFinish = async (e)=> {
        e.preventDefault();
        const id = e.target.value

        const fin = `http://localhost:8080/api/appointments/${id}/finished/`
        const can = `http://localhost:8080/api/appointments/${id}/cancel/`

        const url = e.target.name === 'finished'? fin : can

        const options = {
          method: 'put',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify(id)
        };

        const response = await fetch(url, options);

        if (response.ok) {
            getServices();
            navigate("/services")

            }else{
                console.log("Error posting")
        }
    }

    useEffect(() => {
        getServices();
    }, []);

    return(
        <table className="table">
  <thead>
    <tr>
        {headers.map((header, index) => {
            return(
                <th key={index} scope="col">{header}</th>
            )
        })}
    </tr>
  </thead>
  <tbody>
        {submittedAppointments.map((service,index) =>{
            return(
                <tr key={service.id}>
                <th scope="row">{index + 1}</th>

                <td>{service.vin}</td>
                <td>{x.includes(service.vin)? 'YES': "NO"}</td>
                <td>{service.customer}</td>
                <td>{service.date_time.split('T')[0]}</td>
                <td>{service.date_time.split('T')[1].split('+')[0]}</td>
                <td>{service.technician.first_name} {service.technician.last_name}</td>
                <td>{service.reason}</td>
                <td><button onClick={setFinish} name='finished' value={service.id} type="button" className="btn btn-success">Finished</button></td>
                <td><button onClick={setFinish} value={service.id} name='cancel' type="button" className="btn btn-danger">Cancel</button></td>
                </tr>
            )
        })}
  </tbody>
</table>
    )
}

export default ServiceListView;
