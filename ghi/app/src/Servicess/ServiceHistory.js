import { GetServices } from "./FetchServices"
import { useState } from "react"

 export const HistoryList = ({ autos}) => {
    const [search, setSearch] = useState('')
    const headers=[
        'Item #',
        'VIN',
        'Is VIP?',
        'Customer',
        'Date',
        'Time',
        'Technician',
        'Reason',
        'Status',
    ]

    const services = GetServices()
    const x = []
    autos.map(a => x.push(a.vin))

    const handleChange = (e) => {
        e.preventDefault();
        setSearch(e.target.value);

      };

    const services1 = services.filter(service => service.vin.startsWith(search))


    return(
        <>
        <div className="mt-3">
      <input onChange={handleChange} className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />

    </div>
        <table className="table table-striped">
  <thead>
    <tr>
        {headers.map((header, index) => {
            return(
                <th key={index} scope="col">{header}</th>
            )
        })}
    </tr>
  </thead>
  <tbody>
        {services1.map((service,index) =>{
            return (
              <tr key={service.id}>
                <th scope="row">{index + 1}</th>
                <td>{service.vin}</td>
                <td>{x.includes(service.vin) ? "YES" : "NO"}</td>
                <td>{service.customer}</td>
                <td>{service.date_time.split("T")[0]}</td>
                <td>{service.date_time.split("T")[1].split("+")[0]}</td>
                <td>{`${service.technician.first_name}  ${service.technician.last_name}`}</td>
                <td>{service.reason}</td>
                <td>{service.status.status}</td>
              </tr>
            );
        })}
  </tbody>
</table>
</>
    )
}
