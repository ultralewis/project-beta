import React from "react";
import { useEffect, useState} from "react";
import ServiceListView from "./DisplayActiveServices"
import HistoryList from "./ServiceHistory";
import { CreateService } from "./CreateServiceForm";

export const GetServices = () =>{

    const [appointments, setServices] = useState([])

    const serviceData =async () => {
        const url = 'http://localhost:8080/api/appointments/'
        const getData = await fetch(url);

        const data = await getData.json();
        setServices(data.appointments)
    }

    useEffect(()=>{
        serviceData()
    },[])

    return(
        appointments
    )
}
