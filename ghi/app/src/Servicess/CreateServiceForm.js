import React from "react";
import { useEffect, useState } from "react";
import { GetTechs } from "../Techs/FetchTechs";
import { useNavigate } from "react-router-dom";
import { GetServices } from "./FetchServices";
export const CreateService = ({})=> {

    const status =[
        {'status': 'Finished', 'id':2},
        {'status': 'Canceld', 'id':3},
        {'status': 'Created', 'id':4}
    ]

    const techs = []
    GetTechs().map(t =>{techs.push({'first':t.first_name, 'last':t.last_name, 'id':t.employee_id})})

  const navigate = useNavigate();


    const[data, setData] =useState({
      vin:"",
      customer: "",
      date_time: "",
      technician: "",
      reason: "",
    });


    const handleChangeName = (event) => {
      const type = event.target.type;
      const name = event.target.name;
      const value = event.target.value
      setData(curData => ({
          ...curData,
          [name] : value
      }));
    }

    const postData = async (e)=> {
      console.log(data)
      e.preventDefault();


      const url = `http://localhost:8080/api/appointments/`

      const options = {
        method: 'post',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(data)
      };

      const response = await fetch(url, options);

      if (response.ok) {

        navigate(`/services/`);

    }else{
      console.log("Error posting")
    }
  }
  return (
    <div className="container mt-5">
    <h1 className="text-center mb-4">Create Service </h1>
      <form onSubmit={postData} className="">
    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="text"  name="vin" className="form-control" id="vinnumber" aria-describedby="lastname" placeholder=" Vin" />
    </div>

    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="text"  name="customer" className="form-control" id="customername" aria-describedby="lastname" placeholder=" Customer Name" />
    </div>

    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="datetime-local"  name="date_time" className="form-control" id="date" aria-describedby="date" placeholder="date" />
    </div>

    <div className="form-group mb-2">
      <input onChange={handleChangeName} type="text"  name="reason" className="form-control" id="reason" aria-describedby="reason" placeholder="reason" />
    </div>

    <div className="dropdown mb-2 ">
  <label className="m-2"> </label>
  <select className="container" onChange={handleChangeName} name="technician">
    <option value="" hidden>Choose a technician</option>
  {techs.map(tech=> {return(
     <option value={tech.id} key={tech.id}>{`${tech.first} ${tech.last}`}</option>
  )})}
    </select>
   </div>

    <button  type="submit" className="btn mt-2 btn-primary ">Submit</button>
    </form>
    </div>

  )
  }
