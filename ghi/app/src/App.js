import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import { useEffect, useState } from 'react';
import AllModelss from './CarModel/FetchCarModels';
import ModelForm from './CarModel/ModelForm';
import { CreateService } from './Servicess/CreateServiceForm';
import ServiceListView from './Servicess/DisplayActiveServices';
import { CreateTech } from './Techs/CreateTechForm';
import { TechListView } from './Techs/DisplayTechs';
import { HistoryList } from './Servicess/ServiceHistory';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import SalespersonHistoryList from './SalespersonHistoryList';


function App() {


  const [manufacturers, setManufacturers] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [autos, setAutomobiles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);

  const [appointments, setServices] = useState([]);


  const getServices = async () => {
    const servicesUrl = "http://localhost:8080/api/appointments/";
    const response = await fetch(servicesUrl);

    if (response.ok) {
    const {appointments} = await response.json();
    setServices(appointments);
    };
  };

  const getManufacturers = async () => {
    const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturersUrl);

    if (response.ok) {
      const {manufacturers} = await response.json();
      setManufacturers(manufacturers);
    };
  };

  const getSalespeople = async () => {
    const salespeopleUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salespeopleUrl);

    if (response.ok) {
      const { salespeople } = await response.json();
      setSalespeople(salespeople);
    };
  };

  const getAutomobiles = async () => {
    const automobilesUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(automobilesUrl);

    if (response.ok) {
      const { autos } = await response.json();
      setAutomobiles(autos);
    };
  };

  const getCustomers = async () => {
    const customersUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customersUrl);

    if (response.ok) {
      const {customers} = await response.json();
      setCustomers(customers);
    };
  };

  const getSales = async () => {
    const salesUrl = "http://localhost:8090/api/sales/";
    const response = await fetch(salesUrl);

    if (response.ok) {
      const {sales} = await response.json();
      setSales(sales);
    };
  };

  useEffect(() => {
    getManufacturers();
    getSalespeople();
    getAutomobiles();
    getCustomers();
    getSales();

    getServices();
  }, []);

  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models">
            <Route index element= {<AllModelss />} />
            <Route path='add' element= {<ModelForm manufacturers={manufacturers}/>} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={manufacturers}/>} />
            <Route path="create" element={<ManufacturerForm getManufacturers={getManufacturers}/>} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList autos={autos}/>} />
            <Route path="create" element={<AutomobileForm getAutomobiles={getAutomobiles}/>} />
          </Route>
          <Route path= "techs">
            <Route index  element={<TechListView/>} />
            <Route path='newtech' element={<CreateTech />} />
          </Route>
          <Route path= "services">
            <Route index element={<ServiceListView appointments={appointments} autos={autos} getServices={getServices}/>} />
            <Route path='history' element={<HistoryList autos={autos}/>} />
            <Route path='create' element={<CreateService />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespersonList salespeople={salespeople}/>} />
            <Route path="create" element={<SalespersonForm getSalespeople={getSalespeople}/>} />
          </Route>
          <Route path="customers">
            <Route index element = {<CustomerList customers={customers}/>} />
            <Route path="create" element={<CustomerForm getCustomers={getCustomers}/>} />
          </Route>
          <Route path="sales">
            <Route index element = {<SalesList sales={sales} />} />
            <Route path="create" element={<SalesForm getAutomobiles={getAutomobiles} getSales={getSales} salespeople={salespeople} customers={customers}/>} />
            <Route path="history" element={<SalespersonHistoryList sales={sales} salespeople={salespeople}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>

  );
};

export default App;
