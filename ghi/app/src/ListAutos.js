import React from "react";
import { useEffect, useState} from "react";

const DisplayAutos = () => {
    const [cars, setCars] = useState([])
    const headers = [
        'Vin',
        'Color',
        'Year',
    ]

    const x = cars.map(car =>{

    })

    const allCars = async () => {
        const url = 'http://localhost:8100/api/automobiles/'

        const getData = await fetch(url);
        const data = await getData.json();
        setCars(data.autos)

    }
    useEffect(()=>{
        allCars()
    },[])

    return (

         <table className="table">
  <thead className="thead-dark">
  <tr>
    {headers.map((header, id) => {
        return (
        <th key={id} scope="col">{header}</th>
        )
    })}
    </tr>
  </thead>
  <tbody>

      {cars.map((car, index) => {
        return (
            <tr key={index}>
            <th scope="row">{index +1}</th>
            <td>{car.vin}</td>
            <td>{car.color}</td>
            <td>{car.year}</td>
            <td>{car.model.name}</td>
            <td>{car.model.manufacturer.name}</td>
            <td>{car.sold? 'Yes':'No' }</td>
            </tr>
        )
    })}
   </tbody>
 </table>

    )

}
export default DisplayAutos
