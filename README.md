# CarCar

CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.

Team:

* Darnell Calahan - Auto Services
* Lewis Jeon - Auto Sales

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<https://gitlab.com/ultralewis/project-beta/-/tree/main>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

## Design

CarCar is made up of 3 microservices which interact with one another.

- **Inventory** - Inventory API can be accessed from browser or Insomnia on port 8100 and polling service on port 8000.
- **Services** - Services API can be accessed from browser or Insomnia on port 8080.
- **Sales** - Sales API can be accessed from browser or Insombia on port 8090.

![Img](/images/CarCarDiagram.png)


## Integration - How we put the "team" in "team"

Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible.

How this all starts is at our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.

## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

JSON body to send data:

Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Toyota"
}
```
The return value of creating, viewing, updating a single manufacturer:
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Toyota"
}
```
Getting a list of manufacturers return value:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Toyota"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Honda"
		}
  ]
}
```
### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "4Runner",
  "picture_url": "https://www.image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "5Runner",
  "picture_url": "https://www.image.yourpictureurl.com/example/"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "5Runner",
  "picture_url": "https://www.image.yourpictureurl.com/example/",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Toyota"
  }
}
```
Getting a List of Vehicle Models Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
   	  "id": 1,
	  "name": "5Runner",
	  "picture_url": "https://www.image.yourpictureurl.com/example/",
	  "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Toyota"
	  }
	},
	{
	  "href": "/api/models/2/",
	  "id": 2,
	  "name": "Civic Type R",
	  "picture_url": "https://www.image.yourpictureurl.com",
	  "manufacturer": {
	    "href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Honda"
	  }
	}
  ]
}
```
### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
	"color":"red",
	"year": 2023,
	"vin": "1GNSCBKC8FR582394",
	"model_id":1
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1GNSCBKC8FR582394/",
	"id": 1,
	"color": "red",
	"year": 2023,
	"vin": "1GNSCBKC8FR582394",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "5Runner",
		"picture_url": "https://www.image.yourpictureurl.com/example/",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Toyota"
		}
	},
	"sold": false
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1GNSCBKC8FR582394/

Return Value:
```
{
	"href": "/api/automobiles/1GNSCBKC8FR582394/",
	"id": 1,
	"color": "red",
	"year": 2023,
	"vin": "1GNSCBKC8FR582394",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "5Runner",
		"picture_url": "https://www.image.yourpictureurl.com/example/",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Toyota"
		}
	},
	"sold": false
}
```
You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```
Getting a list of Automobile Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1GNSCBKC8FR582394/",
      "id": 1,
      "color": "red",
      "year": 2012,
      "vin": "1GNSCBKC8FR582394",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "5Runner",
        "picture_url": "https://www.image.yourpictureurl.com/example/",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Toyota"
        }
      },
      "sold": false
    }
  ]
}
```

## Design

## Services microservice

The service microservice is an extension of the dealership that looks to provide service repairs for your vehicle.

As automobiles are purchased, we keep track of the VIN number of each automobile and you are able to receive the special perks of being a VIP! If you bring ina car that wasnt't purchased, don't worry, you are more than welcome to bring in your car for services.

As a VIP, you will receive free oil changes for life, complimentary neck massages while in our waiting room, and free car washes whenever you would like!

This area is going to be broken down into the various API endpoints (Fancy way of saying your web address url) for service along with the format needed to send data to each component.
The basics of service are as follows:
1. Our friendly technician staff members
2. Service Appointments

### Technicians - The heart of what we do here at CarCar

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/id/

LIST TECHNICIANS: Following this endpoint will give you a list of all technicians that are currently employed.

Since this is a GET request, you do not need to provide any data.
```
Example:
{
	"techs": [
		{
			"href": "/api/technicians/1/",
			"first_name": "Albert",
			"last_name": "Kim",
			"employee_id": 8,
			"id": 1
		},
		{
			"href": "/api/technicians/2/",
			"first_name": "Bob",
			"last_name": "Lee",
			"employee_id": 123,
			"id": 2
		}
	]
}
```
CREATE TECHNICIAN - To create a technician, you would use the following format to input the data and you would just submit this as a POST request.
```
{
	"first_name": "Charlie",
	"last_name": Park",
	"employee_id": 1000
}
```

DELETE TECHNICIAN - To delete a technician, you do not need to input any data but will have to specify the technician ID in the url. Please note in this case, the technician id is "id", not "employee_id". In case for Bob Lee mentioned above, you would use "2" (without the quotation marks). The return value of deleting a technician is:
```
{
	"delete": true
}
```
### Service Appointments: We'll keep you on the road and out of our waiting room

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<int:id>

LIST SERVICE APPOINTMENT: This will return a list of all service appointments which includes outstanding, completed and cancelled appointments.

This is the format that will be displayed.

Spoiler alert! Remember, the way that it is returned to you is the way that the data needs to be accepted. Remember, the "id" is automatically generated, so you don't need to input that.
Also, the "date" and "time" fields HAVE TO BE IN THIS FORMAT. In front-end, there will be a date/time input assistant where you can chhose the date and time, so there's no required format in front-end.
```
{
	"appointments": [
		{
			"href": "/api/appointments/2/",
			"date_time": "2023-12-21T13:08:00+00:00",
			"reason": "Some Reason...",
			"customer": "Charlie Kim",
			"vin": "5TDJK3EH5AS027031",
			"id": 2,
			"technician": {
				"href": "/api/technicians/1/",
				"first_name": "Albert",
				"last_name": "Kim",
				"employee_id": 8,
				"id": 1
			},
			"status": {
				"status": "Waiting",
				"id": 4
			}
		},
		{
			"href": "/api/appointments/8/",
			"date_time": "2023-12-13T08:24:00+00:00",
			"reason": "Some Reason...",
			"customer": "David Kim",
			"vin": "1FTNE24W76HB30854",
			"id": 8,
			"technician": {
				"href": "/api/technicians/2/",
				"first_name": "BOB",
				"last_name": "Lee",
				"employee_id": 123,
				"id": 2
			},
			"status": {
				"status": "Complete",
				"id": 2
			}
		}
	]
}
```
SERVICE APPOINTMENT DETAIL: This will return the detail of each specific service appointment.
```
{
	"appointments": {
		"href": "/api/appointments/2/",
		"date_time": "2023-12-21T13:08:00+00:00",
		"reason": "1 Reason",
		"customer": "Charlie Kim",
		"vin": "5TDJK3EH5AS027031",
		"id": 2,
		"technician": {
			"href": "/api/technicians/1/",
			"first_name": "Albert",
			"last_name": "Kim",
			"employee_id": 8,
			"id": 1
		},
		"status": {
			"status": "Waiting",
			"id": 4
		}
	}
}

CREATE SERVICE APPOINTMENT - This will create a service appointment with the data input. It must follow the format. Remember, the "id" is automatically generated, so don't fill that in. To verify
that it was added, just look at your service appointment list after creating a service appointment and it should be there.
```
{
	"vin": "1222",
	"customer_name": "Gary Lewis",
	"date_time": "2023-12-21T13:08:00+00:00",
	"reason": "Any reason",
	"technician": 1
}

SERVICE APPOINTMENT HISTORY: This will show the detail based on the "VIN" that is input. You will see ALL service appointments for the vehicle associated with the "vin" that you input. This was done on front-end application.

## Sales microservice

On the backend, the sales microservice has 4 models: Salesperson, Customer, AutomobileVO and Sale. Sale is the model that interacts with the other three models. This model gets data from the three other models.

Salesperson model has 3 fields - first_name(CharField), last_name(CharField) and employee_id(CharField).

Customer model has 4 fields - first_name(CharField), last_name(CharField), address(TextField) and phone_number(CharField).

AutomobileVO has 2 fields - vin(CharField where value has to be unique) and sold(BooleanField where default value is False). The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

Sale model has 4 fields - price(CharField), automobile(ForeignKey to AutomobileVO model), salesperson(ForeignKey to Salesperson model) and customer(ForeigKey to Customer model).

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.

## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/


To create a salesperson (SEND THIS JSON BODY):
```
{
	"first_name": "Jane",
    "last_name": "Doe",
	"employee_id": "JDoe"
}
```
Return Value of creating a salesperson:
```
{
    "employee_id": "JDoe",
    "first_name": "Jane",
    "last_name": "Doe",
	"id": 1
}
```
List all salespeople Return Value:
```
{
	"salespeople": [
		{
			"employee_id": "JDoe",
			"first_name": "Jane",
			"last_name": "Doe",
			"id": 1
		},
        {
			"employee_id": "LBird",
			"first_name": "Larry",
			"last_name": "Bird",
			"id": 3
		},
		{
			"employee_id": "DFisher",
			"first_name": "Derek",
			"last_name": "Fisher",
			"id": 4
		}
	]
}
```
Return Value of deleting a Salesperson if salesperson exist:
```
{
	"delete": true
}
```
Return Value of deleting a Salesperson if salesperson DOES NOT exist:

        404 Not Found
```
{
	"message: ": "Salesperson does not exist"
}
```
### Customers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
	"first_name": "Stephen",
	"last_name": "Curry",
	"address": "844 Dunbar Street, Morgantown, WV 26508",
	"phone_number": "649-934-2519"
}
```
Return Value of Creating a Customer:
```
{
	"first_name": "Stephen",
	"last_name": "Curry",
	"phone_number": "649-934-2519",
	"address": "844 Dunbar Street, Morgantown, WV 26508",
	"id":1

}
```
Return value of Listing all Customers:
```
{
	"customers": [
		{
			"first_name": "Stephen",
			"last_name": "Curry",
			"phone_number": "649-934-2519",
			"address": "844 Dunbar Street, Morgantown, WV 26508",
			"id": 1
		},
		{
			"first_name": "Klay",
			"last_name": "Thompson",
			"phone_number": "845-364-4739",
			"address": "555 Kingston Drive, Berwyn, IL 60402",
			"id": 2
		}
	]
}
```
Return Value of deleting a customer if customer exist:
```
{
	"delete": true
}
```
Return Value of deleting a customer if customer DOES NOT exist:

        404 Not Found
```
{
	"message: ": "Customer does not exist"
}
```
### Salesrecords:
- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales (all automobiles sold) | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Delete a sale | DELETE | http://localhost:8090/api/sales/id/

List all Salesrecords Return Value:
```
{
	"sales": [
		{
			"price": "$20,000.00",
			"automobile": {
				"vin": "1GNSCBKC8FR582394",
				"sold": true
			},
			"salesperson": {
				"employee_id": "JDoe",
				"first_name": "Jane",
				"last_name": "Doe",
				"id": 1
			},
			"customer": {
				"first_name": "Klay",
				"last_name": "Thompson",
				"phone_number": "845-364-4739",
				"address": "555 Kingston Drive, Berwyn, IL 60402",
				"id": 2
			},
			"id": 1
		},
		{
			"price": "$9,293.23",
			"automobile": {
				"vin": "1GTR2VE7XDZ358826",
				"sold": true
			},
			"salesperson": {
				"employee_id": "DFisher",
				"first_name": "Derek",
				"last_name": "Fisher",
				"id": 4
			},
			"customer": {
				"first_name": "Kevon",
				"last_name": "Loooney",
				"phone_number": "272-877-5136",
				"address": "9105 Valley View Dr., Acworth, GA 30101",
				"id": 7
			},
			"id": 2
		},
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
	"price": "$9,293.23",
	"automobile": "1GTR2VE7XDZ358826",
	"salesperson": 4,
	"customer": 7
}
```
Return Value of Creating a New Sale:
```
{
	"price": "$9,293.23",
	"automobile": {
		"vin": "1GTR2VE7XDZ358826",
		"sold": true
	},
	"salesperson": {
		"employee_id": "DFisher",
		"first_name": "Derek",
		"last_name": "Fisher",
		"id": 4
	},
	"customer": {
		"first_name": "Kevon",
		"last_name": "Loooney",
		"phone_number": "272-877-5136",
		"address": "9105 Valley View Dr., Acworth, GA 30101",
		"id": 7
	},
	"id": 4
}
```
Return Value of deleting a sale if sale exist:
```
{
	"delete": true
}
```
Return Value of deleting a sale if sale DOES NOT exist:

        404 Not Found
```
{
	"message: ": "Sale does not exist"
}
```
