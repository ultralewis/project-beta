from django.urls import path
from .views import tech_api, tech_delete, appointment_api, app_stat_can, get_single_app, app_stat_fin

urlpatterns = [
    path('technicians/', tech_api, name='tech_list'),
    path('technicians/<int:id>/', tech_delete, name='tech_lists'),
    path('appointments/', appointment_api, name='appointment_api'),
    path('appointments/<int:id>/', get_single_app, name='appointments_api'),
    path('appointments/<int:id>/cancel/', app_stat_can, name='cancel_api'),
    path('appointments/<int:id>/finished/', app_stat_fin, name='appoint_finised_api'),
]
