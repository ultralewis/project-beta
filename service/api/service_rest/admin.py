from django.contrib import admin
from .models import Status, Technician, Appointment, AutomobileVO


# Register your models here.
admin.site.register(Status)
admin.site.register(Technician)
admin.site.register(Appointment)
admin.site.register(AutomobileVO)
