from django.shortcuts import render
from .models import Appointment, Status, Technician
from  common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

# Create your views here.

class StatusEncoder(ModelEncoder):
    model = Status
    properties = [
        "status",
        "id"
    ]

class TechEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "customer",
        "vin",
        "id",
        "technician",
        "status",


    ]

    encoders = {
        "technician": TechEncoder(),
        "status": StatusEncoder(),
    }

@require_http_methods(["GET", "POST"])
def appointment_api(request):
    if request.method == "GET":
        appoint = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appoint},
            encoder=AppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            appoint = content["technician"]
            tech = Technician.objects.get(employee_id=appoint)
            content["technician"] = tech
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the Appointment"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def get_single_app(request,id):
    if request.method == "GET":
        appoint = Appointment.objects.get(id=id)
        return JsonResponse(
            {"appointments": appoint},
            encoder=AppointmentEncoder,
        )



@require_http_methods(["GET", "POST"])
def tech_api(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=TechEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            techs = Technician.objects.create(**content)
            return JsonResponse(
                techs,
                encoder=TechEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the Technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def tech_delete(request, id):
    if request.method == "DELETE":
        count, _ = Technician.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(['PUT'])
def app_stat_can(request,id):
    if request.method == 'PUT':
        try:
            # content = jsodon.loads(request.body)
            app = Appointment.objects.get(id=id)
            status = Status.objects.get(id=3)
            setattr(app, "status", status)
            app.save()
            return JsonResponse(
                app,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(['PUT'])
def app_stat_fin(request,id):
    if request.method == 'PUT':
        try:
            # content = json.loads(request.body)
            app = Appointment.objects.get(id=id)
            status = Status.objects.get(id=2)
            setattr(app, "status", status)
            app.save()
            return JsonResponse(
                app,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
