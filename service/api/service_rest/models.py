from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    sold = models.BooleanField()


class Status(models.Model):
    status = models.CharField(max_length=50)

    def __str__(self):
        return (f'{self.status} - {self.id}')



class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return (f'{self.first_name} - {self.last_name}')
    def get_api_url(self):
        return reverse('tech_lists', kwargs={'id': self.id})


class Appointment(models.Model):
    date_time=models.DateTimeField()
    reason = models.TextField()
    customer = models.CharField(max_length=100)
    vin = models.CharField(max_length=50)

    status = models.ForeignKey(
        Status,
        related_name='appointment',
        on_delete=models.PROTECT,
        default=4,
        blank=True
    )


    technician = models.ForeignKey(
        Technician,
        related_name='appointment',
        on_delete=models.PROTECT

    )

    def get_api_url(self):
        return reverse('appointments_api', kwargs={'id': self.id})
    def __str__(self):
        return self.customer
