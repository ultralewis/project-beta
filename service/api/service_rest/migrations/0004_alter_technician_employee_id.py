# Generated by Django 4.0.3 on 2023-12-20 05:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_appointment_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='technician',
            name='employee_id',
            field=models.PositiveIntegerField(unique=True),
        ),
    ]
